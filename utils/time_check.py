import datetime
import os
import pickle

time_check_file = os.path.join(os.path.dirname(__name__), 'time_check_IGNORE.pickle')

def query_time_check(query, pause_length=12):
    """
    Checks when was the last time we looked up a query.
    """
    current_datetime = datetime.datetime.now()
    if os.path.exists(time_check_file):
        with open(time_check_file, 'rb') as tcf:
            time_dict = pickle.load(tcf)
        if query in time_dict:
            last_time_check = time_dict[query]
        else:
            query_save_time(query)
            return True
        time_diff = current_datetime - last_time_check
        if time_diff < datetime.timedelta(hours=pause_length):
            return False
        return True
    else:
        query_save_time(query)
        return True

def query_save_time(query):
    """
    Saves the time of query for a given query.
    """
    time_dict = {}
    if os.path.exists(time_check_file):
        with open(time_check_file, 'rb') as tcf:
            time_dict = pickle.load(tcf)
    time_dict[query.lower()] = datetime.datetime.now()
    with open(time_check_file, 'wb') as tcf:
        pickle.dump(time_dict, tcf)


def query_get_last_time(query):
    """
    Retrieves the last time when a query was checked.
    """
    if os.path.exists(time_check_file):
        with open(time_check_file, 'rb') as tcf:
            time_dict = pickle.load(tcf)
            if query in time_dict:
                return time_dict[query.lower()].isoformat(' ')
    query_save_time(query)
    query_get_last_time(query)
