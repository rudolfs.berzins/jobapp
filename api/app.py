import os
import sys
from flask import (
    Flask,
    jsonify,
    make_response,
    request,
    abort
)
sys.path.append(os.path.dirname("."))
import settings
from scrapers.scrapers import (
    BaseScraper,
    available_scrapers
)
from utils.time_check import (
    query_time_check,
    query_save_time,
    query_get_last_time
)
app = Flask(__name__)

@app.route("/scraper/api/all", methods=["POST"])
def scrape_all():

    if not request.json and not 'query' in request.json:
        abort(400)
    query = request.json['query']

    errors = []
    if query_time_check(query):
        BS = BaseScraper(query)
        for name, scraper in available_scrapers().items():
            try:
                BS(scraper)
            except Exception as e:
                errors.append((name, e))
        query_save_time(query)
    else:
        return make_response(jsonify({'message': f'Skipping scrape. Last scrape {query_get_last_time(query)}.'}))

    if not errors:
        return make_response(jsonify({'message':f'Scrapes for {query} successful!'}, 201))
    return make_response(jsonify(
        {'error':
            [f'Error {e} occured in {name} scraper' for name, e in errors]
        }
    ), 401)

@app.route("/scraper/api/<string:scraper_name>", methods=["POST"])
def scrape_individual(scraper_name):

    if not request.json and not 'query' in request.json:
        abort(400)
    query = request.json['query']

    errors = []
    all_scrapers = available_scrapers()
    if scraper_name not in all_scrapers:
        return make_response(jsonify({'error': 'Scraper not found'}), 401)
    scraper = all_scrapers[scraper_name]
    try:
        BS = BaseScraper(query)
        BS(scraper)
    except Exception as e:
        errors.append(e)
    if not errors:
        return make_response(jsonify({'message':f'Scrape in {scraper_name} for {query} successful!'}, 201))
    return make_response(jsonify(
        {'error':
            f'Error {e} occured in {name} scraper' for name, e in errors
        }
    ), 401)

@app.route("/scraper/api/all", methods=["GET"])
def show_all_scrapers():
    return jsonify({'scrapers': [scraper_name for scraper_name in available_scrapers()]})




