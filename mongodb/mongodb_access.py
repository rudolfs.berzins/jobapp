import os
import pymongo
import functools

@functools.lru_cache()
def _initialize_database(database_name='test'):
    client = pymongo.MongoClient(os.environ["MONGODB_JOBAPP"], ssl=True)
    db = client[database_name]
    return db

@functools.lru_cache()
def _initialize_collection(collection_name='test_collection', database_name='test'):
    db = _initialize_database(database_name)
    collection = db[collection_name]
    return collection

def _insert_scraping_results(result_list, target_collection):
    insert_ids = target_collection.insert_many(result_list).inserted_ids
    return len(insert_ids)

def _check_if_entry_in_collection(entry, target_collection):
    find_result = target_collection.find_one({"hash_key": entry['hash_key']})
    return False if find_result else True

def check_and_add_results(result_list, collection_name='test_collection', database_name='test'):
    collection = _initialize_collection(collection_name, database_name)
    filter_func = functools.partial(_check_if_entry_in_collection, target_collection=collection)
    result_list = list(filter(filter_func, result_list))
    if result_list:
        num_inserts = _insert_scraping_results(result_list, collection)
        print(f"Inserted {num_inserts} in {collection_name} in database {database_name}")
    else:
        print(f"All entries already in {collection_name} in database {database_name}")

    return True
