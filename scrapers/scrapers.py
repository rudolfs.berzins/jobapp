import hashlib
import sys
import os
from datetime import datetime, timedelta
from pprint import pprint
from bs4 import BeautifulSoup

import settings
try:
    from scrapers.base_functions import normalize_entry, save_to_json, simple_get, shorten_url
except ModuleNotFoundError:
    from base_functions import normalize_entry, save_to_json, simple_get, shorten_url
from mongodb.mongodb_access import check_and_add_results

class BaseScraper():
    """
    A base class for scraper object.
    """
    def __init__(self, query, upload=True):
        self.query = query
        self.upload = upload
        self.col_name = "Job_Collection"
        self.db_name = "Job_Postings"

    def __call__(self, scraper):
        """Make base class callable"""
        postings = scraper(self)
        self.upload_to_DB(postings)

    def upload_to_DB(self, postings):
        """Upload to MongoDB"""
        check_and_add_results(
            postings,
            collection_name=self.col_name,
            database_name=self.db_name
        )


def cv_lv_scraper(base_scraper):
    """
    Scrape jobs from https://www.cv.lv
    """

    url = f"https://www.cv.lv/darba-sludinajumi/q-{base_scraper.query}"
    response = simple_get(url)
    postings = []
    if response is not None:
        page = BeautifulSoup(response, "html.parser")
        divs = page.find_all("div", attrs={"class": "cvo_module_offer"})

        for i, div in enumerate(divs):
            entry_dict = {}
            main_content = div.find("div", attrs={"class": "offer_content"})
            entry_dict["title"] = normalize_entry(main_content.h2.a.contents[0])
            posting_link = main_content.h2.a["href"]
            entry_dict["link"] = posting_link
            try:
                salary_string = main_content.find(
                    "li", attrs={"class": "offer-salary"}
                ).text
            except AttributeError:
                salary_string = None
            entry_dict["salary_numbers"] = (
                list(
                    map(
                        int,
                        filter(
                            lambda x: x.isdigit() and x != "00",
                            salary_string.replace(".", " ").split(),
                        ),
                    )
                )
                if salary_string
                else None
            )
            entry_dict["company"] = normalize_entry(
                main_content.find(
                    "span", attrs={"itemprop": "hiringOrganization"}
                ).a.text
            )
            entry_dict["location"] = normalize_entry(
                main_content.find("span", attrs={"itemprop": "jobLocation"}).a.text
            )
            start_date_string = main_content.find(
                "span", attrs={"itemprop": "datePosted"}
            )["content"]
            entry_dict["start_date"] = datetime.strptime(start_date_string,'%Y-%m-%d')
            end_date_string = (
                main_content.find("i", attrs={"class": "fa-clock-o"})
                .parent.parent["title"]
                .strip()
                .replace(".", "-", 2)[:-1]
            )
            entry_dict["end_date"] = datetime.strptime(end_date_string,'%Y-%m-%d')
            entry_dict["source"] = "cv_lv"
            entry_dict["liked"] = False
            entry_dict["add_date"] = datetime.utcnow()
            entry_dict["tags"] = ["cv_lv", datetime.utcnow().strftime('%Y-%m-%d'), base_scraper.query.lower()]
            entry_dict["query"] = base_scraper.query.lower()
            entry_dict["hash_key"] = hashlib.sha256("".join([
                    str(i) for i in [
                        entry_dict["title"], entry_dict["company"], entry_dict["start_date"].date()
                    ]
                ]).encode("utf-8")).hexdigest()
            postings.append(entry_dict)
    return postings


def jobindex_scraper(base_scraper):
    """
    Scrape jobs from https://www.jobindex.dk/
    """


    query = base_scraper.query.replace(" ", "+")
    page_num = 1
    url = "https://www.jobindex.dk/jobsoegning?page={}&q={}"
    postings = []
    while True:
        target_page = url.format(page_num, query)
        page_num += 1
        response = simple_get(target_page)
        if response:
            page = BeautifulSoup(response, "html.parser")
            paid_job_divs = [("paid_job", div) for div in page.find_all("div", attrs={"class": "PaidJob"})]
            robot_job_divs = [("robot_job", div) for div in page.find_all("div", attrs={"class": "jix_robotjob"})]
            divs = paid_job_divs + robot_job_divs
            if not len(divs):
                break
            for post_type, div in divs:
                entry_dict = {}
                if post_type == "paid_job":
                    title_company_links = [link for link in div.find_all("a") if link.b]
                    entry_dict["title"] = title_company_links[0].b.text
                    posting_link = title_company_links[0]['href']
                    entry_dict["link"] = posting_link
                    entry_dict["company"] = title_company_links[1].b.text
                    try:
                        entry_dict["location"] = div.find("p").text.split(',')[1].strip()
                    except IndexError as identifier:
                        entry_dict["location"] = None

                elif post_type == "robot_job":
                    title_link_element = div.find("a")
                    entry_dict["title"] = title_link_element.strong.text
                    posting_link = title_link_element['href']
                    if len(posting_link) > 100:
                        posting_link = shorten_url(posting_link)
                    entry_dict["link"] = posting_link
                    try:
                        entry_dict["company"], entry_dict["location"] = [bold_element.text for bold_element in div.find_all("b")]
                    except ValueError:
                        entry_dict["company"] = div.find("b").text
                        entry_dict["location"] = None
                pub_date_element = div.find("li", attrs={"class": "toolbar-pubdate"})
                entry_dict["start_date"] = datetime.strptime(pub_date_element.time['datetime'], '%Y-%m-%d')
                entry_dict["salary_numbers"] = None
                entry_dict["end_date"] = None
                entry_dict["source"] = "jobindex"
                entry_dict["liked"] = False
                entry_dict["add_date"] = datetime.utcnow()
                entry_dict["tags"] = ["jobindex", datetime.utcnow().strftime('%Y-%m-%d'), query.lower()]
                entry_dict["query"] = query.lower()
                entry_dict["hash_key"] = hashlib.sha256("".join([
                    str(i) for i in [
                        entry_dict["title"], entry_dict["company"], entry_dict["start_date"].date()
                    ]
                ]).encode("utf-8")).hexdigest()
                postings.append(entry_dict)
    return postings


def indeed_scraper(base_scraper):
    """
    Scrape jobs from https://dk.indeed.com
    """

    query = base_scraper.query.replace(" ", "-")
    start_pos = 0
    link_template = "https://dk.indeed.com"
    url = "{}/jobs?q={}&start={}"
    postings = []
    while True:
        target_page = url.format(link_template, query, start_pos)
        response = simple_get(target_page)
        if response:
            page = BeautifulSoup(response, "html.parser")
            page_postings = page.find_all("div", attrs={"class": "result"})
            if not len(page_postings):
                break
            for posting in page_postings:
                entry_dict = {}
                title_link_element = posting.find("h2", attrs={"class": "title"}).find("a")
                entry_dict["title"] = title_link_element.text.strip()
                entry_dict["link"] = link_template + title_link_element["href"]
                company_loc_element = posting.find("div", attrs={"class": "sjcl"})
                entry_dict["company"] = company_loc_element.find("span", attrs={"class": "company"}).text.strip()
                try:
                    entry_dict["location"] = company_loc_element.find("div", attrs={"class": "location"}).text.strip()
                except AttributeError:
                    entry_dict["location"] = None

                entry_dict["add_date"] = datetime.utcnow()

                date_element = posting.find("span", attrs={"class": "date"}).text
                if "dage" in date_element:
                    days_before = [int(x) for x in date_element.split(" ") if x.isdigit()][0]
                    entry_dict["start_date"] = datetime.today() - timedelta(days=days_before)
                else:
                    entry_dict["start_date"] = datetime.today()
                entry_dict["salary_numbers"] = None
                entry_dict["end_date"] = entry_dict["start_date"] + timedelta(days=30)
                entry_dict["source"] = "indeed"
                entry_dict["liked"] = False
                entry_dict["tags"] = ["indeed", entry_dict["add_date"].strftime('%Y-%m-%d'), query.lower()]
                entry_dict["query"] = query.lower()
                entry_dict["hash_key"] = hashlib.sha256("".join([
                    str(i) for i in [
                        entry_dict["title"], entry_dict["company"], entry_dict["start_date"].date()
                    ]
                ]).encode("utf-8")).hexdigest()
                postings.append(entry_dict)

            pn_links = page.find_all("span", attrs={"class": "pn"})
            if pn_links[-1].find("span", attrs={"class": "np"}):
                start_pos += 10

            if len(postings) > 100:
                break

    return postings


def graduateland_scraper(base_scraper):

    query = base_scraper.query.replace(" ", "+")
    link_base = "https://graduateland.com"
    last_page_num = None
    url = "{}/jobs?keyword={}&regions[]=10055&limit=10&offset=0".format(link_base, query)
    target_page = url
    postings = []
    while True:
        response = simple_get(target_page)
        if response:
            page = BeautifulSoup(response, "html.parser")
            page_postings = page.find_all("a", attrs={"class": "job-box"})
            if not len(page_postings):
                break
            for posting in page_postings:
                if posting.find("div", attrs={"class": "job-box__meta-info"}) is None:
                    continue
                entry_dict = {}
                entry_dict["link"] = link_base + posting["href"]
                title_element = posting.find("h1", attrs={"class": "job-box__heading"})
                entry_dict["title"] = title_element.text.strip().split("\n")[0]
                entry_dict["location"] = posting.find("div", attrs={"class": "job-box__meta-info"}).find_all("div")[-1].text.strip()
                entry_dict["company"] = posting.find("span", attrs={"class": "job-box__company-name"}).text.strip()

                entry_dict["add_date"] = datetime.utcnow()

                entry_dict["start_date"] = datetime.today()
                entry_dict["salary_numbers"] = None
                entry_dict["end_date"] = entry_dict["start_date"] + timedelta(days=30)
                entry_dict["source"] = "graduateland"
                entry_dict["liked"] = False
                entry_dict["tags"] = ["graduateland", entry_dict["add_date"].strftime('%Y-%m-%d'), query.lower()]
                entry_dict["query"] = query.lower()
                entry_dict["hash_key"] = hashlib.sha256("".join([
                    str(i) for i in [
                        entry_dict["title"], entry_dict["company"], entry_dict["start_date"].date()
                    ]
                ]).encode("utf-8")).hexdigest()
                postings.append(entry_dict)

            # Navigate to next page
            pagination_div = page.find("div", attrs={"class": "pagination"})
            all_page_links = pagination_div.find_all("a")
            if last_page_num is None:
                last_page_num = all_page_links[-2].text.strip()
            active_page = pagination_div.find("a", attrs={"class": "pagination__item--active"})
            if last_page_num == active_page.text.strip():
                break  # We have reached an parsed the last page

            # Always take the last link
            target_page = link_base + all_page_links[-1]["href"]

    return postings


def available_scrapers():
    """
    Returns a dict containing all available scrapers.
    """
    return {
        'CVLV': cv_lv_scraper,
        'JobIndexDK': jobindex_scraper,
        'IndeedDK': indeed_scraper,
        'GraduateLand': graduateland_scraper
    }



if __name__ == "__main__":
    BS = BaseScraper("python")
    BS(graduateland_scraper)