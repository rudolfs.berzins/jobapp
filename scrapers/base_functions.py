import json
from contextlib import closing
from unicodedata import normalize, combining

from requests import RequestException, get, utils


def simple_get(url):
    """
    Attempts to get the content at `url` by making an HTTP GET request.
    If the content-type of response is some kind of HTML/XML, return the text
    content, otherwise return None.
    """

    headers = utils.default_headers()
    headers.update(
        {
            "User-Agent": "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:52.0) Gecko/20100101 Firefox/52.0"
        }
    )

    try:
        with closing(get(url, headers, stream=True)) as resp:
            if is_good_response(resp):
                return resp.content
            else:
                return None

    except RequestException as e:
        log_error(f"Error during requests to {url}:{str(e)}")
        return None


def save_to_json(postings, file_name):
    with open(file_name, "w") as fn:
        json.dump(postings, fn, indent=4)


def is_good_response(resp):
    """
    Returns True if response seems to be HTML, False otherwise.
    """
    content_type = resp.headers["Content-Type"].lower()
    return (
        resp.status_code == 200
        and content_type is not None
        and content_type.find("html") > -1
    )


def normalize_entry(entry):
    norm_txt = normalize("NFD", entry)
    shaved = ''.join(c for c in norm_txt if not combining(c))
    return normalize("NFC", shaved)

def shorten_url(given_url):
    tiny_url = f"https://tinyurl.com/api-create.php?url={given_url}"
    with closing(get(tiny_url, stream=True)) as resp:
        return resp.content.decode()


def log_error(e):
    """
    This function just prints errors.
    """
    print(e)